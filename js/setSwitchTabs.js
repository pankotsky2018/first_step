export function setSwitchTabs (TabsParentClassName, TabsContentParentClassName) {
    const tabs = document.querySelector(TabsParentClassName);
    const tabsContentChildrens = document.querySelector(TabsContentParentClassName).children;
    const tabsChildrens = document.querySelector(TabsParentClassName).children;
    
    [...tabsChildrens].forEach(child => child.dataset.tabs = child.textContent);

    for (let i = 0; i < tabsContentChildrens.length; i++) {
        if (tabsChildrens.length < tabsContentChildrens.length){
            for (let i = 0; i < tabsChildrens.length; i++){
                tabsContentChildrens[i].dataset.tabs = tabsChildrens[i].dataset.tabs;
            }
        } else {
            tabsContentChildrens[i].dataset.tabs = tabsChildrens[i].dataset.tabs;
        }
    }

    tabs.addEventListener('click', e => {
        [...tabsChildrens].forEach(child => {
            if (child !== e.target) {
                child.classList.remove('active');
                [...tabsContentChildrens].forEach(child => {
                    child.classList.remove('active');
                })
            }
        })
        e.target.classList.add('active');
        [...tabsContentChildrens].forEach(child => {
            if (e.target.dataset.tabs === child.dataset.tabs) {
                child.classList.add('active');
            }
        })
    })
}