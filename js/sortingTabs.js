export function sortingTabs (TabsParentClassName, TabsContentParentClassName) {
    const tabs = document.querySelector(TabsParentClassName);
    const tabsContentChildrens = document.querySelector(TabsContentParentClassName).children;
    const tabsChildrens = document.querySelector(TabsParentClassName).children;
    const loadButton = document.querySelector('.button-load-more');
    
    
    loadButton.addEventListener('click', () => {
        [...tabsContentChildrens].forEach(child => {
            child.classList.add('active');
            child.dataset.visible = 'true';
        });
        loadButton.style.display = 'none';
    }, {once:'true'});
    
    [...tabsChildrens].forEach(child => child.dataset.tabs = child.textContent);
    
    tabs.addEventListener('click', e => {
        [...tabsChildrens].forEach(child => {
            if (child !== e.target) {
                child.classList.remove('active');
                [...tabsContentChildrens].forEach(child => {
                    child.classList.remove('active');
                });
            };
        });
        e.target.classList.add('active');
        [...tabsContentChildrens].forEach(child => {
            if (e.target.dataset.tabs === child.dataset.tabs && child.dataset.visible === 'true') {
                child.classList.add('active');
            };
            if (e.target.dataset.tabs === 'All' && child.dataset.visible === 'true') {
                child.classList.add('active');
            };
        });
    });
}